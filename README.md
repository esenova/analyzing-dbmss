# **Analyzing selected big database management systems (DMBSs):

## ***Discuss the architecture and main components of the selected DBMSs:***

- MYSQL
- CASSANDRA

## **ARCHITECTURE OF MYSQL**

Client-server architecture is used by MySQL. It is set up so that clients, who are end users, can utilise a variety of networking services to access the server computer's resources. The mejor layers of the MySQL architecture are as follows:

- Client
- Server
- Storage Layer

![Figure 1](mysqlarch.png)

## **ARCHITECTURE OF CASSANDRA**

- Has no master or slave nodes because of the way it is built. 
- It features a ring-type architecture, meaning that its nodes are logically dispersed in the shape of a ring.
- Distribution of data among all nodes occurs automatically.
- Data is copied between the nodes for redundancy, just like HDFS.
- Multiple data centres are supported by the Cassandra architecture.
- Across data centres, data can be copied.

![Figure 2](cassandra.png)

## **COMPONENTS OF MYSQL**

- ***Thread handling:*** A link is formed to signify the formation of that connection if the server approves a request submitted from the MYSQL database client-end. This link enables the user to execute any further commands. A connection cannot be established when a command is entered incorrectly since the server will typically reject it. 

- ***Parser:*** As soon as the data is entered into the database, a procedure called lexical analysis separates it into a number of tokens. The server's parser is a piece of software that assists in creating a data structure from the inputted data. 

- ***The Optimizer:*** After the parser has built the data structure, the optimizer is used in the server system. The generated data structure is subjected to optimization techniques including query rewriting and table scanning. 

- ***Buffer and cache:*** The cache and buffer hold the commands and their results after MYSQL statements are input and their results are processed. The server searches through this record whenever a new command entry is made to check and existing command result. If a result already exists, it sends that information instead of starting the process over to handle a new result. 

- ***Table metadata cache:*** The server area where data of data are stored is called metadata cache. Database, index, and object information is stored in the metadata cache. The size of the metadata grows as the size of the data input does. 

- ***Key cache:*** A key cache is an index entry that uniquely identifies a specific object in a cache. 

## **COMPONENTS OF CASSANDRA**

- ***Node:*** The location where data is kept is called a Cassandra node

- ***Data Center:*** Is a group of connected nodes

- ***Cluster:*** Is a part that includes one or more data centers

- ***Commit log:*** The commit log in Cassandra serves as a crash-recovery mechanism. The commit log records each writing operation. 

- ***Mem-table:*** Is a data structure that resides in memory. The information will be written to the main mem-table following the commit log. Sometimes there will be more than one mem-table for a single-column family.

- ***SSTable:*** When its contents reach a certain value, data is flushed from the mem-table to this disc file. 

- ***Bloom Filter:*** For determining if an element is a member of a set, there are only rapid, nondeterministic algorithms. It is a unique type of cache. Following every query, bloom filters are accessible. 

# **What types of big data and how the selected systems can handle them?**

![Figure 3](typesofbigdata.png)

Here are 11 tips for working with large data sets:

- *Cherish your data:* teal advises us to "keep your raw data raw" by keeping our data in a location where backups are made automatically and other lab members can access it while following by your institution's consent and data privacy policies.

- *Visualize the information:* according to Titus Brown, a bioinformation at the University of California, Davis, as data sets grow, additional kinks appear.

- *Show your workflow:* by documenting every stage of the data processing process, including the data version you used, the cleaning and quality-checking procedures, and any processing code you used. Such details are crucial for recording and reusing your approaches. 

- *Use version control:* version control systems enable researchers to pinpoint the specific changes made to a file over time and identify the contributors. However, some systems have restrictions on the file sizes you can employ.


